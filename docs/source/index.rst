Jogos Python
==============================================
Jogos Python é um pacote que permite a criação de jogos de um jeito mais fácil, usando pygame por baixo dos panos.

Instalação
==============================================
Para instalar é preciso ter o gerenciador de pacotes ``pip``, que normalmente vem instalado junto com o Python (mas pode ser instalado separadamente, se preciso) e usar o comando ``pip install jogos_python``.

Primeiros Passos
==============================================

Após a instalação da biblioteca, você pode usá-la no seu script com ``import jogos-python as jp``.

Para acessar constantes, funções e classes, é necessário usar um prefixo na frente. Se você usar o comando acima, ``jp`` é o prefixo. Caso você omita e use apenas ``import jogos-python``, o prefixo será ``jogos-python`` (não recomendamos, pois fica muito grande).

Exemplos de uso:
::
    import jogos-python as jp

    tela = jp.abrir_tela(cor=jp.verde)

    galinha = jp.Objeto('galinha')

    galinha.tecla_mover_para_esquerda = jp.tecla_seta_para_esquerda
    galinha.tecla_mover_para_direita = jp.tecla_seta_para_direita
    galinha.tecla_mover_para_cima = jp.tecla_seta_para_cima
    galinha.tecla_mover_para_baixo = jp.tecla_seta_para_baixo

    while True:
        galinha.mover()

Observe que, para criar o objeto 'galinha', usamos ``jp.Objeto('galinha')``, mas depois de criado, usamos apenas ``galinha``, uma variável do seu programa, para fazer referência ao objeto criado.

Funções auxiliares
======
Funções que não estão ligadas a uma classe específica, mas podem ser usadas no seu código. (Lembre-se de incluir o prefixo antes dessas funções, explicado em Primeiros Passos)

esperar( tempo ):
    Faz o código demorar `tempo` segundos antes de prosseguir. `tempo` também pode ser um número decimal, por exemplo, 0.5.

definir_frequencia( frequencia ):
    Quando você precisa de uma frequência de execução de determinada ação. Analogo de `definir_periodo`, não devem ser usados juntos.

definir_periodo( periodo ):
    Quando você precisa de um intervalo entre uma execução e outra de determinada ação. Analogo de `definir_frequencia`, não devem ser usados juntos.

Pausar/Despausar

    pausar( tempo=False ):
        Pausa o jogo. Se fornecido o parâmetro `tempo`, irá pausar por `tempo` segundos. Caso contrário, deve-se usar a função `despausar` para "despausar" manualmente o jogo.

    despausar():
        "Despausa" o jogo. Desfaz o que `pausar` faz.

Tela
======
Classe da variável retornada, ao usar a seguinte função:

abrir_tela()

    Abre uma nova tela/janela. Deve ser o primeiro comando usado após importar a biblioteca.

    abrir_tela(largura=800, altura=600, cor = roxo, fps= 60) -> Tela

    **Tipo dos Parâmetros:**
    
        Tamanho

            largura <int> - largura da tela

            altura <int> - altura da tela
            
        cor <tuple> - cor de fundo da tela. Existem variáveis na biblioteca, com o nome de algumas cores. Lembre-se de incluir o prefixo antes dessas cores (explicado em Primeiros Passos)
        
        fps <int> - taxa de atualização da tela, em frames por segundo

    **Propriedades da Classe:**

        Tamanho

            largura <int> - largura da tela aberta pela função, não pode ser alterada.

            altura <int> - altura da tela aberta pela função, não pode ser alterada.

        cor <int> - cor de fundo da tela aberta pela função.

        fps <int> - taxa de atualização da tela, em frames por segundos. 

Cores
============
Cores pré-definidas dentro da biblioteca: (Lembre-se de incluir o prefixo antes dessas cores, explicado em Primeiros Passos)

    vermelho, 
    
    verde, 
    
    azul, 
    
    azul_escuro, 
    
    verde_escuro, 
    
    roxo, 
    
    amarelo, 
    
    laranja, 
    
    rosa, 
    
    cinza, 
    
    marrom, 
    
    preto

Teclas
==========

Funções relacionadas às teclas:

    tecla_apertada( tecla ):
        Função usada para verificar se uma tecla está sendo pressionada naquele momento. O argumento da função deve uma das teclas definidas na biblioteca.
        
        tecla_apertada( tecla ) -> bool

Valores de teclas pré-definidos: (Lembre-se de incluir o prefixo antes dessas teclas, explicado em Primeiros Passos)

    #letras:
        tecla_a,

        tecla_b,
        
        tecla_c, 
        
        tecla_d, 
        
        tecla_e, 
        
        tecla_f, 
        
        tecla_g,
        
        tecla_h, 
        
        tecla_i, 
        
        tecla_j, 
        
        tecla_k, 
        
        tecla_l, 
        
        tecla_s, 
        
        tecla_t, 
        
        tecla_u, 
        
        tecla_v, 
        
        tecla_w, 
        
        tecla_x, 
        
        tecla_y, 
        
        tecla_z 

    #numeros do centro do teclado:
        tecla_0, 
        
        tecla_1, 
        
        tecla_2, 
        
        tecla_3, 
        
        tecla_4, 
        
        tecla_5, 
        
        tecla_6, 
        
        tecla_7, 
        
        tecla_8, 
        
        tecla_9 

    #números do teclado numérico:
        tecla_numero_0, 

        tecla_numero_1, 

        tecla_numero_2, 

        tecla_numero_3, 

        tecla_numero_4, 

        tecla_numero_5, 

        tecla_numero_6, 

        tecla_numero_7, 

        tecla_numero_8, 

        tecla_numero_9 


    #setas:
        tecla_seta_para_esquerda,
        
        tecla_seta_para_direita,
        
        tecla_seta_para_cima,
        
        tecla_seta_para_baixo 

Direções
===========
Direções pré-definidas: (Lembre-se de incluir o prefixo antes dessas direções, explicado em Primeiros Passos)
    para_esquerda = [-1,0],
    
    para_direita = [1,0],
    
    para_cima = [0,-1],
    
    para_baixo = [0,1],
    
    direcao_nula = [0,0]

Objeto
=========
Objeto( nome_da_imagem ):
    
    Classe usada para representar objetos na biblioteca.
    Os objetos podem se mover, colidir com outro objeto e são clicáveis (usando o mouse ou a tela touchscreen) para gerar alguma ação, de forma similar às teclas do teclado.

    **Tipo dos Parâmetros:**
        nome_da_imagem <str> - Nome da imagem, pode ser uma imagem salva no computador do usuário, neste caso, deve ser incluída a extensão. Outra possibilidade é usar uma das imagens da biblioteca, listadas abaixo.

    **Possibilidades para parâmetro nome_da_imagem:**
        'galinha', 'pintinho', 'ovo', 'fazenda'

    Exemplo:
    ::
        import jogos-python as jp

        tela = jp.abrir_tela()

        galinha = jp.Objeto('galinha')

    **Funções da Classe:**
    
        Colisões

            colidiu_com( objeto ):
                Testa se o objeto colidiu com algum outro objeto

            tocou_na_direita( objeto ):
                Testa se o objeto tocou na parte mais à esquerda de outro objeto

            tocou_na_esquerda( objeto ):
                Testa se o objeto tocou na parte mais à direita de outro objeto
            
            tocou_em_cima( objeto ):
                Testa se o objeto tocou em cima de outro objeto

            tocou_embaixo( objeto ):
                Testa se o objeto tocou embaixo de outro objeto

        espelhar( forma = 'horizontalmente' ):
            Espelha imagem. Por padrão, espelha horizontalmente.
            **Possibilidades para parâmetro nome_da_imagem:**
                'horizontalmente','verticalmente'

        apagar():
            Apaga o objeto. Este método faz o objeto sumir, mas sem perder sua referência
            ( nome do objeto ).
        
        Movimentação

            andar( x, y ):
                Permite andar com objeto horizontalmente x unidades e verticalmente y unidades.

            adicionar_movimento( movimento ):
                Adiciona movimento a partir de uma função já criada no programa. 
                Esta função deve ter um parâmetro objeto, que será usado como o objeto usada

            pular( velocidade_de_pulo=100, delay=0.1 ):
                Faz o objeto pular na tela. Você pode mudar a velocidade do pulo e quanto tempo vai demorar (com o delay).

    **Propriedades da Classe:**

        nome_da_imagem <str> - nome da imagem mostrada pelo objeto

        Coordenadas

            x <int> - Define a coordenada horizontal do objeto
            
            y <int> - Define a coordenada vertical do objeto

            posicao <tuple> - Posição do objeto. Corresponde a usar uma lista
            [x,y]

            esquerda <int> - Parte mais à esquerda do objeto

            direita <int> - Parte mais à direita do objeto

            topo <int> - topo do objeto

            base <int> - base do objeto

            centro_x <int> - centro do objeto na direção horizontal

            centro_y <int> - centro do objeto na direção vertical

            centro <tuple> - centro do objeto. Semelhante a criar uma lista 
            [centro_x, centro_y].

        Tamanho

            largura <int> - Define largura do objeto
            
            altura <int> - Define altura do objeto
            
            manter_proporção <bool> - Se for igual a True (verdadeiro), alterar a altura também mudará a largura, para manter a proporção entre as ambas. O mesmo acontece ao mudar largura. Padrão: True

        pressionado <bool> - Apenas leitura. Testar se ele está sendo pressionado.

        existe <bool> - Se for igual a True (verdadeiro), o objeto "existe" (é mostrado na tela). Caso, contrário, o objeto não é mostrado na tela. Para não mostrar na tela, use apagar(). Não confundir com `visivel`. Padrão: True

        visivel <bool> - True se o objeto está vísivel na tela, False caso esteja invisível. Padrão: True

        solido <bool> - Se for igual a True (verdadeiro), o objeto é sólido e reage a colisões com outros objetos. Padrão: True.

        estatico <bool> - Se for igual a True (verdadeiro), o objeto não se mexe em caso de colisão. Padrão: False

        Movimentação
            
            cai <bool> - Se for igual a True (verdadeiro), o objeto sofrerá ação da gravidade e irá cair. Padrão: False
            
            manter_na_tela <bool> - Se for igual a True (verdadeiro), restringe que o objeto saia da tela. Padrão: True

            velocidade <int> - Velocidade que o objeto irá se mover, em pixels por segundo. Padrão: 100
            direção <tuple> - Direção que o objeto está se movendo. Com direcao_nula, ele não se move. Padrão: direcao_nula

            tecla_mover_para_esquerda <tecla> - Define a tecla que fará este objeto se movimentar para esquerda quando pressionada

            tecla_mover_para_direita <tecla> - Define a tecla que fará este objeto se movimentar para direita quando pressionada

            tecla_mover_para_cima <tecla> - Define a tecla que fará este objeto se movimentar para cima quando pressionada

            tecla_mover_para_baixo <tecla> - Define a tecla que fará este objeto se movimentar para baixo quando pressionada

            botao_mover_para_esquerda <Botao | Objeto> - Define o botão ou objeto que fará este objeto se movimentar para esquerda quando pressionado

            botao_mover_para_direita <Botao | Objeto> - Define o botão ou objeto que fará este objeto se movimentar para direita quando pressionado

            botao_mover_para_cima <Botao | Objeto> - Define o botão ou objeto que fará este objeto se movimentar para cima quando pressionado
            
            botao_mover_para_baixo <Botao | Objeto> - Define o botão ou objeto que fará este objeto se movimentar para baixo quando pressionado

Botao
===========
Botao( x=0, y=0, largura=50, altura=50, cor=preto)

    Classe usada para criar e manipular botões na biblioteca.

    Os botões são clicáveis, usando o mouse ou a tela touchscreen e 
    podem ser usados para gerar alguma ação, de forma similar às teclas do teclado.


    **Tipo dos Parâmetros:**

        x <int> - Define a coordenada horizontal do botão
    
        y <int> - Define a coordenada vertical do botão

        largura <int> - Define largura do botão
        
        altura <int> - Define altura do botão

        cor <tuple> - Define a cor do botão

    **Propriedades da Classe:**

        Coordenadas

            x <int> - Define a coordenada horizontal do botão
            
            y <int> - Define a coordenada vertical do botão
        
        Tamanho

            largura <int> - Define largura do botão
            
            altura <int> - Define altura do botão

        cor <tuple> - Define a cor do botão

        pressionado <bool> - Apenas leitura. Testar se ele está sendo pressionado.


Texto
===========
Texto( mensagem, cor=preto, x=0, y=0):
    
    Classe usada para mostrar texto na tela na biblioteca.

    O texto pode ser customizado, ter cores diferentes e ser posicionado na tela.

    **Propriedades da Classe:**

        Coordenadas

            x <int> - Define a coordenada horizontal do texto
            
            y <int> - Define a coordenada vertical do texto

        mensagem <str> - Mensagem que será exibida no texto.

        cor <tuple> - cor do texto
