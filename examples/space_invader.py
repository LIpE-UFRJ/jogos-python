import jogos_python as jp
import random as rd

tela = jp.abrir_tela(largura=500,altura=400)
nave = jp.Objeto('galinha')
nave.direita = tela.largura
nave.tecla_mover_para_cima = jp.tecla_seta_para_cima
nave.tecla_mover_para_baixo = jp.tecla_seta_para_baixo
nave.velocidade = 300

projetil = jp.Objeto('ovo')
projetil.velocidade = 600
projetil.posicao = nave.posicao
projetil.direcao = jp.para_esquerda

inimigo1 = jp.Objeto('pintinho')
inimigo1.direcao = jp.para_direita
inimigo1.velocidade = 60
inimigo1.y = rd.randint( 0, tela.altura )

#Os outros inimigos são repetições do inimigo 1
#No exemplo, ele têm mesma velocidade, mas as velocidades podem ser diferentes
inimigo2 = jp.Objeto('pintinho')
inimigo2.direcao = jp.para_direita
inimigo2.velocidade = 60
inimigo2.y = rd.randint( 0, tela.altura )

inimigo3 = jp.Objeto('pintinho')
inimigo3.direcao = jp.para_direita
inimigo3.velocidade = 60
inimigo3.y = rd.randint( 0, tela.altura )

inimigo4 = jp.Objeto('pintinho')
inimigo4.direcao = jp.para_direita
inimigo4.velocidade = 60
inimigo4.y = rd.randint( 0, tela.altura )

inimigo5 = jp.Objeto('pintinho')
inimigo5.direcao = jp.para_direita
inimigo5.velocidade = 60
inimigo5.y = rd.randint( 0, tela.altura )

while( True ):#jp.obter_estado_do_jogo == 'rodando' ): 
    #atira
    jp.definir_frequencia(100)
    if( jp.esta_apertada(jp.tecla_f) ):
        projetil.posicao = nave.posicao
        
    if( inimigo1.colidiu_com(projetil) ):
        inimigo1.esquerda = 0
        inimigo1.y = rd.randint( 0, tela.altura )
        
    if( inimigo2.colidiu_com(projetil) ):
        inimigo2.esquerda = 0
        inimigo2.y = rd.randint( 0, tela.altura )
        
    if( inimigo3.colidiu_com(projetil) ):
        inimigo3.esquerda = 0
        inimigo3.y = rd.randint( 0, tela.altura )
        
    if( inimigo4.colidiu_com(projetil) ):
        inimigo4.esquerda = 0
        inimigo4.y = rd.randint( 0, tela.altura )
        
    if( inimigo5.colidiu_com(projetil) ):
        inimigo5.esquerda = 0
        inimigo5.y = rd.randint( 0, tela.altura )