import jogos_python as jp

tela = jp.abrir_tela()

objeto = jp.Objeto('galinha')

objeto.posicao = [tela.largura/2, tela.altura/2]

while( True ):
    jp.definir_frequencia(10)
    if(objeto.pressionado):
        objeto.nome_da_imagem = 'pintinho'
    else:
        objeto.nome_da_imagem = 'galinha'
