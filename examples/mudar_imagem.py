import jogos_python as jp
jp.abrir_tela()

objeto = jp.Objeto( 'galinha' )
objeto.posicao = (200,200)

imagens = ['pintinho', 'ovo', 'galinha']

def trocar_imagem( periodo ):
    while( jp.obter_estado_do_jogo() != "fechado" ):
        for i in imagens:
            jp.definir_periodo( periodo )
            objeto.nome_da_imagem = i


jp.rodar_em_paralelo( trocar_imagem, [2] )
