import jogos_python as jp 

tela = jp.abrir_tela()

jp.gravidade = 15

chao0 = jp.Objeto("caixa")
chao0.y = 500
chao0.manter_proporcao = False

chao1 = jp.Objeto("caixa")
chao1.y = 500
chao1.manter_proporcao = False

espaco = 150
chao0.largura = 300
chao1.largura = tela.largura - chao0.largura - espaco
chao1.direita = tela.largura

galinha = jp.Objeto("galinha")

galinha.base = chao0.topo
galinha.tecla_mover_para_direita = jp.tecla_d
galinha.tecla_mover_para_esquerda = jp.tecla_a

galinha.tecla_pular = jp.tecla_espaco
galinha.cai = True


while( jp.obter_estado_do_jogo() != "fechado" ):
#pode ser trocado por while True, mas dá erro quando fecha
    jp.definir_frequencia(10)#deixa jogo mais fluido
    if( jp.esta_apertada( galinha.tecla_pular) ):
        if( galinha.tocou_em_cima(chao0) or
            galinha.tocou_em_cima(chao1)):
            galinha.pular( 150 )