# Inicío
import jogos_python as jp
import random
tela = jp.abrir_tela(800,600)

# variaveis - global
naveI = jp.Objeto("pintinho")
naveI.visivel = False
balaP = jp.Objeto("ovo")
balaP.visivel = False

# Nave Principal - criação
naveP = jp.Objeto("galinha")
naveP.espelhar('horizontalmente')
naveP.manter_proporcao = True
naveP.largura = 125
naveP.altura = 125
naveP.centro_y = 275
naveP.posicao = [25, 268]

# Nave Principal - movimentação
naveP.tecla_mover_para_cima = jp.tecla_seta_para_cima
naveP.tecla_mover_para_baixo = jp.tecla_seta_para_baixo
naveP.velocidade = 425

# ovorincipal - tiro
def tiro():
    if jp.esta_apertada(jp.tecla_a):
        
        jp.esperar(0.1)
    
        balaP = jp.Objeto("ovo")
        balaP.visivel = True
    
        balaP.manter_proporcao = True
        balaP.largura = 35
        balaP.altura = 35
        balaP.posicao = [naveP.centro_x + 30, naveP.centro_y - 15]
        balaP.solido = False
    
        balaP.direcao = jp.para_direita
        balaP.adicionar_movimento(colisao)
        balaP.velocidade = 980
        
        
# Nave Inimiga - aparecer
def inimigos():
    jp.esperar(4)
    
    for i in range(3):
        
        naveI = jp.Objeto("pintinho")
        naveI.visivel = True
        naveI.manter_proporcao = True
        naveI.largura = 110
        naveI.altura = 110
        naveI.posicao = [525,random.randint(0,600)]
        
        naveI.direcao = jp.para_esquerda
        naveI.velocidade = 100
        
# Função - colisão geral
def colisao( objeto ):
    if len(objeto.colisoes) > 0:
        for i in objeto.colisoes:
            if( i != naveP ):
                i.apagar()
                objeto.apagar()
    elif( objeto.direita >= tela.largura ):
        objeto.apagar()
                
jp.adicionar_acao(inimigos)
jp.adicionar_acao(tiro)
