import jogos_python as jp
import random as rd

tela = jp.abrir_tela(400,400)

placar = jp.Texto(0, jp.amarelo)
placar.posicao = [tela.largura/2, 20]

caixa = jp.Objeto('caixa')

ovo = jp.Objeto('ovo')

margem = 30

caixa.velocidade = 100
caixa.posicao = [10,10]
caixa.direcao = jp.para_direita
ovo.posicao = [rd.randint(margem,tela.largura-margem-ovo.largura),\
                rd.randint(margem,tela.altura-margem-ovo.altura)]

while( jp.obter_estado_do_jogo() == 'rodando'):#True ):
    jp.definir_frequencia(caixa.velocidade)
    #mudar direcao
    if( jp.esta_apertada(jp.tecla_seta_para_cima) ):
        caixa.direcao = jp.para_cima
    if( jp.esta_apertada(jp.tecla_seta_para_baixo) ):
        caixa.direcao = jp.para_baixo
    if( jp.esta_apertada(jp.tecla_seta_para_esquerda) ):
        caixa.direcao = jp.para_esquerda
    if( jp.esta_apertada(jp.tecla_seta_para_direita) ):
        caixa.direcao = jp.para_direita
        
    #mover ovo
    if( caixa.colidiu_com( ovo ) ):
        ovo.posicao = [rd.randint(margem,tela.largura-margem-ovo.largura),\
                rd.randint(margem,tela.altura-margem-ovo.altura)]
        caixa.velocidade += 10
        placar.mensagem += 7
        
        
    #morrer
    if( caixa.esquerda == 0 or caixa.topo == 0 or \
        caixa.direita == tela.largura or caixa.base == tela.altura):
        jp.pausar(3)
        caixa.velocidade = 100
        caixa.posicao = [10,10]
        caixa.direcao = jp.para_direita
        ovo.posicao = [rd.randint(margem,tela.largura-margem-ovo.largura),\
                   rd.randint(margem,tela.altura-margem-ovo.altura)]
        placar.mensagem = 0
