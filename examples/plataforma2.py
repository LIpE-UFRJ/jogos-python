import jogos_python as jp

tela = jp.abrir_tela()

chao = jp.Objeto('caixa')

chao.base = tela.altura - 40
chao.manter_proporcao = False
chao.largura = tela.largura

galinha = jp.Objeto('galinha')

galinha.cai = True
galinha.tecla_mover_para_direita = jp.tecla_d
galinha.tecla_mover_para_esquerda = jp.tecla_a
galinha.tecla_saltar = jp.tecla_espaco
galinha.velocidade = 90

def saltar( objeto ):
    if( objeto.pousou and jp.esta_apertada( objeto.tecla_saltar ) ):
        objeto.pular( 150 )
        
galinha.adicionar_movimento( saltar )

def reiniciar():
    galinha.base = chao.topo
    galinha.esquerda = 0
    galinha._velocidade_de_queda = 0

reiniciar()

pintinho = jp.Objeto('pintinho')
pintinho.base = chao.topo
pintinho.direita = tela.largura

def colisao():
    if( galinha.colidiu_com( pintinho ) ):
        jp.pausar(0.2)
        reiniciar()
        
jp.adicionar_acao( colisao )
        

obstaculo = jp.Objeto('caixa')

obstaculo.altura = 100
obstaculo.centro_x = tela.largura/2
obstaculo.base = chao.topo 
