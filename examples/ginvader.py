import jogos_python as jp

jp.abrir_tela(cor=jp.azul, largura=400, altura= 300)

galinha1 = jp.Objeto("galinha")
galinha1.x = 10
galinha1.y = 30
galinha1.velocidade = 300
galinha1.tecla_mover_para_baixo = jp.tecla_s
galinha1.tecla_mover_para_cima = jp.tecla_w
galinha1.tecla_atirar = jp.tecla_j
galinha1.direcao_de_disparo = jp.para_direita
galinha1.espelhar( "horizontalmente" )

galinha2 = jp.Objeto("galinha")
galinha2.x = 350
galinha2.y = 30
galinha2.velocidade = 300
galinha2.tecla_mover_para_baixo = jp.tecla_seta_para_baixo
galinha2.tecla_mover_para_cima = jp.tecla_seta_para_cima
galinha2.tecla_atirar = jp.tecla_numero_5
galinha2.direcao_de_disparo = jp.para_esquerda


pontos1 = jp.Texto(0, jp.preto)
pontos2 = jp.Texto(0, jp.preto)
pontos2.x = 350

galinha1.alvo = galinha2
galinha1.pontos = pontos1

galinha2.alvo = galinha1
galinha2.pontos = pontos2


def auto_destruir( objeto ):
    acertou = objeto.colidiu_com( objeto.alvo )
    if( objeto.x > objeto.x_max or
    objeto.x < objeto.x_min or
    acertou ):
        objeto.apagar()
        if( acertou ):
            if( objeto.pontos.mensagem < 5 ):
                objeto.pontos.mensagem += 1
            elif( objeto.alvo.alvo.existe ):
                objeto.alvo.apagar()

def atirar( objeto ):
    if( jp.esta_apertada( objeto.tecla_atirar ) ):
        ovo = jp.Objeto("ovo")
        ovo.solido = False
        ovo.manter_na_tela = False
        ovo.velocidade = 200
        if( objeto.direcao_de_disparo == jp.para_direita ):
            ovo.esquerda = objeto.direita
        else:
            ovo.direita = objeto.esquerda
        ovo.y = objeto.y
        ovo.direcao = objeto.direcao_de_disparo
        ovo.x_max = 390
        ovo.x_min = 0
        ovo.alvo = objeto.alvo
        ovo.pontos = objeto.pontos
        ovo.adicionar_movimento( auto_destruir )
        jp.esperar(0.2)

galinha1.adicionar_movimento( atirar )
galinha2.adicionar_movimento( atirar )