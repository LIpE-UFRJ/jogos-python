import jogos_python as jp 

tela = jp.abrir_tela()
tela.fps = 120

jp.gravidade = 15

chao = []
for i in range( 2 ):
    caixa = jp.Objeto("caixa")
    caixa.y = 500
    caixa.manter_proporcao = False
    chao.append( caixa )

espaco = 150
chao[0].largura = 300
chao[1].largura = tela.largura - chao[0].largura - espaco
chao[1].direita = tela.largura

galinha = jp.Objeto("galinha")

galinha.base = chao[1].topo
galinha.tecla_mover_para_direita = jp.tecla_d
galinha.tecla_mover_para_esquerda = jp.tecla_a
galinha.virada_para = 'direita'

galinha.tecla_pular = jp.tecla_espaco
galinha.cai = True

def pular( objeto ):
    if( jp.esta_apertada( objeto.tecla_pular) ):
        for i in chao:
            if( objeto.tocou_em_cima(i) ):
                galinha.pular( 150 )
                
def virar( objeto ):
    if( objeto.virada_para == 'direita' and\
        jp.esta_apertada( objeto.tecla_mover_para_esquerda ) ):
        objeto.virada_para = 'esquerda'
        objeto.espelhar('horizontalmente')
    elif( objeto.virada_para == 'esquerda' and\
        jp.esta_apertada( objeto.tecla_mover_para_direita ) ):
        objeto.virada_para = 'direita'
        objeto.espelhar('horizontalmente')

galinha.adicionar_movimento( pular )
galinha.adicionar_movimento( virar )